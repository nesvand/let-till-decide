import webpack from 'webpack';
import path from 'path';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import ImageminPlugin from 'imagemin-webpack-plugin';

const context = path.join(__dirname, 'src');
const staticPath = path.join(__dirname, 'static');
const distPath = path.join(__dirname, 'dist');

const production = process.env.NODE_ENV === 'production';

let plugins = [
	new HtmlWebpackPlugin({
		template: path.join(__dirname, 'static', 'till', 'index.html'),
		filename: 'till/index.html'
	}),
	new ExtractTextPlugin('[name].styles.css')
];

if (production) {
	plugins = [
		new webpack.LoaderOptionsPlugin({
			minimize: true,
			debug: false,
			options: {
				context: context
			}
		}),
		new webpack.optimize.UglifyJsPlugin({
			output: {
				comments: false
			}
		}),
		new ImageminPlugin({
			pngquant: {
				quality: '65-90'
			}
		}),
		new CopyWebpackPlugin([
			{
				from: staticPath
			},
		],
			{
				ignore: [
					'**/till/index.html',
					'**/till/**/*.png'
				]
			})
	].concat(plugins);
}

module.exports = {
	context,
	plugins,	
	entry: {
		app: [
			'babel-polyfill',
			'./js/app.js'
		]
	},
	output: {
		path: distPath,
		publicPath: production ? 'http://nesvand.space/' : 'http://localhost:9000/',
		filename: production ? '[name]-[hash].bundle.js' : '[name].bundle.js',
		sourceMapFilename: production ?  '[name]-[hash].bundle.map' : '[name].bundle.map',
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'babel-loader',
						options: {
							retainLines: true
						}
					}
				]
			},
			{
				test: /\.css$/,
				exclude: /node_modules/,
				use: ExtractTextPlugin.extract({
					fallbackLoader: 'style-loader',
					loader: 'css-loader'
				})
			},
			{
				test: /\.html$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'html-loader',
						options: {
							attrs: [
								'img:src',
								'link:href'
							],
							minimize: true,
							root: staticPath
						}
					}
				]
			},
			{
				test: /\.png$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'url-loader',
						options: {
							limit: 10000,
							mimetype: 'image/png',
							outputPath: 'assets/'
						}
					}
				]
			}
		]
	},
	resolve: {
		extensions: ['.webpack-loader.js', '.web-loader.js', '.loader.js', '.js'],
		modules: [
			path.resolve(__dirname, 'node_modules'),
			context
		]
	},
	devtool: production ? 'source-map' : 'cheap-module-eval-source-map',
	devServer: {
		contentBase: [
			staticPath,
			distPath
		],
		compress: true,
		port: 9000,
		host: '0.0.0.0'
	}
};