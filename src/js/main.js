import WebAudio from './sound.js';

export default app;

// Initialise Web Audio API
const AudioContext = window.webkitAudioContext || window.AudioContext;

// Global constants
const APP_PATH = getAppPath();
const JA = Symbol();
const NEIN = Symbol();
const STRINGS = {
	[JA]: 'JA',
	[NEIN]: 'NEIN'
};
const TEXT_TIMEOUT = 1250;

// Global config
const APP_CONFIG = {
	audioContext: new AudioContext(),
	audioFileMetadata: [
		{
			id: JA,
			url: `${APP_PATH}/assets/audio/ja.mp3`
		},
		{
			id: NEIN,
			url: `${APP_PATH}/assets/audio/nein.mp3`
		}
	],
	audioObjects: {},
	override: getQuery(),
	rootElement: document.body,
	responseElement: document.getElementById('til-response')
};

// Global variables
let TEXT_TIMEOUT_ID;


function app () {
	bootstrap(
		APP_CONFIG.audioFileMetadata,
		APP_CONFIG.audioContext,
		APP_CONFIG.audioObjects,
		startAppFactory(APP_CONFIG)
	);
}

/**
 * @param {Object} audioFileMetadata contains ids and urls of files to process
 * @param {AudioContext} context for processing audio
 * @param {Object} globalAudioObjects stores the processed audio
 * @param {Function} startApp runs once we've loaded and stored the audio
 */
function bootstrap(audioFileMetadata, context, globalAudioObjects, startApp) {
	const storeAudio = storeAudioFactory(audioFileMetadata, globalAudioObjects);
	const webAudioObjects = audioFileMetadata.map(audioFileMeta => new WebAudio(audioFileMeta.url, context));

	getAudioObjects(webAudioObjects)
		.then(storeAudio)
		.then(startApp);
}

function startAppFactory({
	audioContext,
	_audioFileMetas,
	audioObjects,
	override,
	rootElement,
	responseElement
}) {
	return startApp;

	function startApp () {
		const soundPlayer = soundPlayerFactory(audioContext, audioObjects);

		// We have to handle every event under the sun because... 'Web Apps' -_-
		if ('onpointerdown' in window) {
			rootElement.addEventListener('pointerdown', run);
		}
		else {
			rootElement.addEventListener('mousedown', run);

			if ('ontouchstart' in window) {
				rootElement.addEventListener('touchstart', run);
			}
		}

		if (override) {
			run(override);
		}

		/**
		 * @param {String} override to force a specific decision
		 */
		function run (override) {
			updateText(
				makeDecision(soundPlayer, override),
				responseElement
			);
		}
	}
}

/**
 * @param {Object} audioFileMetadata to provide keys for the global audio Object
 * @param {Object} globalAudioObjects holds the audio in the global space
 * @returns {storeAudio}
 */
function storeAudioFactory(audioFileMetadata, globalAudioObjects) {
	return storeAudio;

	/**
	 * @param {Object} loadedAudioObjects to be stored in the global space
	 */
	function storeAudio (loadedAudioObjects) {
		loadedAudioObjects.forEach(
			(loadedAudioObject, index) => globalAudioObjects[audioFileMetadata[index].id] = loadedAudioObject
		);
	}
}

/**
 * @returns {Boolean}
 */
function coinFlip() {
	return Math.floor(Math.random() * 2) === 0;
}

/**
 * @returns {String}
 */
function getAppPath() {
	let path = document.location.pathname.replace(/[^\/]*.html$/, '');

	return (path.substr(-1) === '/') ? path.slice(0, -1) : path;
}

/**
 * @returns {String}
 */
function getQuery() {
	return document.location.search.substring(1).toUpperCase();
}

/**
 * @param {WebAudio[]} files in WebAudio format to be downloaded and processed
 * @returns {Promise}
 */
function getAudioObjects(files) {
	return Promise.all(files.map(file => file.get()))
		.then(audiobuffers => Promise.all(audiobuffers.map((buffer, ii) => files[ii].decode(buffer))));
}

/**
 * @param {String} decision that has been made
 * @param {HTMLElement} element to be updated
 */
function updateText(decision, element) {
	element.innerHTML = STRINGS[decision];

	if (TEXT_TIMEOUT_ID) window.clearTimeout(TEXT_TIMEOUT_ID);

	TEXT_TIMEOUT_ID = window.setTimeout(() => element.innerHTML = '', TEXT_TIMEOUT);
}

/**
 * @param {AudioContext} context for processing audio
 * @param {Object} audioObjects stored in the global audio object
 * @returns {soundPlayer}
 */
function soundPlayerFactory(context, audioObjects) {
	return soundPlayer;

	/**
	 * @param {String} key to locate audio object
	 * @returns {String}
	 */
	function soundPlayer (key) {
		const source = context.createBufferSource();

		source.buffer = audioObjects[key];
		source.connect(context.destination);
		source.start(0);

		return key;
	}
}

/**
 * @param {soundPlayer} soundPlayer function
 * @param {String} [override] to force a specific result
 * @returns {String}
 */
function  makeDecision(soundPlayer, override) {
	const ja = soundPlayer.bind(null, JA);
	const nein = soundPlayer.bind(null, NEIN);

	switch (override) {
	case STRINGS[JA]:
		return ja();
	case STRINGS[NEIN]:
		return nein();
	default:
		return coinFlip() ? ja() : nein();
	}
}
