export default class WebAudio {
	constructor(url, context) {
		this.url = url;
		this.context = context;
	}

	get() {
		return _ajax('GET', this.url);
	}

	decode(arrayBuffer) {
		return _decode(arrayBuffer, this.context);
	}
}

function _ajax(method, url) {
	return new Promise((resolve, reject) => {
		const request = new XMLHttpRequest();

		request.addEventListener('load', () => {
			if (request.status >= 200 && request.status < 300) {
				resolve(request.response);
			}
			else {
				reject(request.response);
			}
		});
		request.addEventListener('error', () => {
			reject(request.response);
		});
		request.responseType = 'arraybuffer';
		request.open(method, url);

		request.send();
	});
}

function _decode(arrayBuffer, context) {
	return new Promise((resolve, reject) => {
		context.decodeAudioData(
			arrayBuffer,
			audioData => {
				resolve(audioData);
			},
			e => reject(e)
		);
	});
}